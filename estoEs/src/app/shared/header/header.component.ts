import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  back: boolean = false;
  routeHeader: string = "/projects";

  constructor(
    private _location: Location,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.router.events.subscribe(e => {
      if(this._location.path() !== "/projects") {
        this.back = true
        this.routeHeader = this._location.path();
      } else {
        this.back = false;
        this.routeHeader = this._location.path();
      }
    })
  }

  lastPage() {
    this._location.back();
    
  }

}
