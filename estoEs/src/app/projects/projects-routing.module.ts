import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddprojectComponent } from './addproject/addproject.component';
import { EditprojectsComponent } from './editprojects/editprojects.component';
import { ProjectsComponent } from './projects.component';

const routes: Routes = [
  { path: '', 
    children: [
      { path: 'projects', component: ProjectsComponent},
      { path: 'projects/add', component: AddprojectComponent},
      { path: 'projects/edit', component: EditprojectsComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
