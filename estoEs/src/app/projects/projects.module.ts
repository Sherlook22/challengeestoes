import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsComponent } from './projects.component';
import { EditprojectsComponent } from './editprojects/editprojects.component';
import { AddprojectComponent } from './addproject/addproject.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjectsRoutingModule } from './projects-routing.module';
import { FormsComponent } from './forms/forms.component';
import { NgxPaginationModule } from 'ngx-pagination';



@NgModule({
  declarations: [
    ProjectsComponent,
    EditprojectsComponent,
    AddprojectComponent,
    FormsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ProjectsRoutingModule,
    NgxPaginationModule
  ],
  exports: [
    ProjectsComponent,
    EditprojectsComponent,
    AddprojectComponent
  ]
})
export class ProjectsModule { }
