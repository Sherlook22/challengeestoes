import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  mobile: boolean = false;
  page: number = 1;

  projects = [
    { projectInfo: "landing page", projectManager:"Walt Cosani", assigned: "Ignaciotrufa"},
    { projectInfo: "landing page", projectManager:"Walt Cosani", assigned: "Ignaciotrufa"},
    { projectInfo: "landing page", projectManager:"Walt Cosani", assigned: "Ignaciotrufa"},
    { projectInfo: "landing page", projectManager:"Walt Cosani", assigned: "Ignaciotrufa"},
    { projectInfo: "landing page", projectManager:"Walt Cosani", assigned: "Ignaciotrufa"},
    { projectInfo: "landing page", projectManager:"Walt Cosani", assigned: "Ignaciotrufa"},
    { projectInfo: "landing page", projectManager:"Walt Cosani", assigned: "Ignaciotrufa"},
    { projectInfo: "landing page", projectManager:"Walt Cosani", assigned: "Ignaciotrufa"}
  ]
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    if (window.screen.width >= 633) { 
      this.mobile = true;
    }
  }

}
