import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {

  projectsForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router
  ) {
    this.projectsForm = this.fb.group({
      projectName: ['', Validators.required],
      description: ['', Validators.required],
      manager: ['', Validators.required],
      assigned: ['', Validators.required],
      status: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }


  setErrors() {
    Object.values( this.projectsForm.controls).forEach((ctls) => {
      ctls.markAsTouched();
    })
  }

  sendProject() {
    if(this.projectsForm.valid){
      alert(`The project has been created`)
      this.router.navigate(['/'])
    }else {
      this.setErrors();
    }
  }

}
